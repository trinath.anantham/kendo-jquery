var express = require("express");
var app = express();

const oneDay = 86400000;
const oneMonth = 30 * 86400000; 
const staticResourceFolders = ['vendor', 'assets', 'images', 'img', 'css', 'font', 'fonts', 'js', 'min', 'sass', 'static']
console.log(__dirname);
staticResourceFolders.forEach(function (staticResourceFolder) {
    app.use('/' + staticResourceFolder, express.static(__dirname + '/public/' + staticResourceFolder, {
        maxAge: oneDay
    }));
});
app.use(express.static(__dirname + '/public'));
app.use('/', (req, res) => {
    res.sendFile('public/html/login.html', { root: '.' });
})
 app.listen(3000);
